# BVA simulation
This is a short user manual for the BVA simulation task. This manual does not include information on how to modify and code the experiment. As the task is built upon [brainvr-unity-framework](https://github.com/BrainVR/brainvr-unity-framework), go to that website for  information on how to edit or implement new paradigms.

# How to use
Tasks are designed to be played from the built player. You should always use the same one throughout the experiment, not necessarily the latest one. It is possible, although I try to prevent that, that some new implementations might play with logging or settings loading in a way that makes past paradigms incompatible. If that is the case, use the old build, or contact me.

## Absolute basics
Build is for Windows machines and should work on both 32 and 64 bit architecture. Good graphics card is not required, you can run it with intel 520/620 integrated graphics, but in that cas I'd lower the settings in graphical fidelity (Check Step 1.)Controls are bound to common WSAD and mouse bindings as in any FPS game. 

## Before using 
Before you use the engine for the first time, read through this manual and email me with any quesions.  Familiarise yourself with the task thoroughly before using it with patients. Do it at least three times from beginning to an end yourselves. Try to give the task to somebody you know before subjecting it to patients! You should have good knowledge of keyboard shortcuts and possible errors, as well as what every screen information etc. means, in case subjects ask.

## Step by step use
1. Open the unity build and check the settings. Try to keep things like graphical fidelity and screen resolution constant. Remember that other people might have been using the PC and changed it. If using several PCs, keep the settings same.
2. After loading screen, you should see main menu. 
3. Set patients name. Each file is automatically tagged with date, so dating it is not necessary. *Names shoudl abide by the OS naming conventions. So no speccial characters or blank spaces*
4. Select wanted settings file and click load. *You shoudl see level name updated as well as name of the experiment appearing in the selection pannel."
5. You can load multiple settings and click on what one to load afterwards. *Note though, that unless you lead different experiemnts, you might not know which experiment is which, as they appear in the pannel under their name"
6. When correct settings file is loaded, click play.
7. Level should load. The expeirment doesn't start yet. 
8. Before experiment starts, provide subject with some beginner instructions. *Make sure that your subject understand how to move, how to use mouse and direction keys. Also, if not running tutorial, explain how the experiment will run*
9. When ready, you can go to pause menu (ESC) and click on Start the experiment. 
10. When the experiment is finished, you can go to pause menu are return to the main menu.
11. You can click on another experiemnt, load new settings and run it from step 6, or just quit.
12. Logs are in ../logs/

## Keyboard shortcuts
You can check and modify most keys in the Input tab during build start. 

- Esc: go to pause menu
- __space__: most of confirmations/starting new trials
- __e/right click__: point
- __u__: unstuck in case player gets stuck
- __i__: shows hidden goal if the patient cannot find it
- __o__: shows starting position
- __p__: force next trial
- __g__: go to next trial (only if possible)
- __F12__: dev console

## Using arduino synchronisation
Using arduino to synchronise iEEG shoudl be straightforward. Plug it in to the usb and to appropriate parts on the iEEG headbox (it doesn't matter which colour wire goes to ref and which to the channel). I recommend starting unity with fake ID (say tesetign_ieeg) and then testing it works and sends signals. In each experiment, arduino sends signals at differnet times, but mostly during start and end of a trial. Run a single trial yourself and check you are getting signals.

## Using Dev console
There is a dev console inside unity. It is useful for some more complex functions that are not bound to particular keys. you can turn it on and off with F12. Enter commands into it and use by pressing enter. The parameters are not passed in parentheses, as listed, but in quotes - *Trial.set(int i)* is in reality called as *Trial.set "int i"*

Commands:
- *.help*: lists all commands available
- *Player.center*: Moves player to the center
- *Experiment.restart*: restarts expeirment - experimental, might screw up logs
- *Trial.set "number"*: quits current trial and starts the trial at given integer
 
## Custom settings
Creating custom settings is fairly simple. Copy some already functional settings and then modify the part inside Settings: [{}]. Validate .json file using some online .json validator, just to be sure. 

# Troubleshooting
__Q: The task does not load.__
A: The most typical problem is that the json settings file is broken. Go and check example .json files at the git repo. Try loading some of the exsample files in the build you got. If they load well, you have a json compiler mistake in your settings file. If not, it is possible the build is broken.

__Q: Scene is flickering__
A: Some more complicated scenes have trouble with lights. Try using the fastest graphical fidelity during startup. Remember that you should keep these settings constant.

__Q: Something goes wrong during the experiemnt and it doesn't work anymore.__
A: If somehting unsolvable goes wrong, there are several options. Either its the PC or the settings file is wrong. If it's the PC (battery, sound), go to next QA. If its the settings, it mmeans it would go wrong again - this shouldn't happen if you tested the experiemnt yourselves. There is nothing I can do :(

__Q: PC crashes during the experiment. I need to restart and don't want to start from the beginning__
A: If that happens, you have to restart the entire experiment, possibly PC. To start from the trial you left of, you can use ForceNextTrial button

__Q: Task loads in the main menu, but level doesn't load when clicking on start.__
A: You probably have build that doesn't work with the settings file. Email me

__Q: Level loads but when I click on play level, nothing happens.__
A: You probably have build that doesn't work with the settings file. Email me
