﻿using BrainVR.UnityFramework.Objects;
using BrainVR.UnityFramework.Scripts.Objects.Marks;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StartManager : ArenaObjectManager<StartManager>
{
    public GameObject MarkPrefab;
    public float DefaultMarkRadius;
    public List<MarkController> Starts { get { return Objects.Cast<MarkController>().ToList(); } }
    public List<MarkController> InstantiateObjectsOnCircleCircumference(int number, int[] positions = null, float radius = 20, Vector3 center = default(Vector3), string MarkType = "")
    {
        //validations
        if (positions != null)
        {
            if (positions.Max() > number - 1)
            {
                Debug.Log("Cannot instantiate marks on positions higher than number of marks");
                return null;
            }
        }
        else
        {
            positions = Enumerable.Range(0, number).ToArray();
        }
        Objects.Clear();
        foreach (var i in positions)
        {
            GameObject mark = Instantiate(MarkPrefab, new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0)) as GameObject;
            if (mark != null)
            {
                mark.transform.SetParent(gameObject.transform);
                mark.SetActive(true);
                MarkController markController = mark.GetComponent<MarkController>();
                Objects.Add(markController);
                if (!string.IsNullOrEmpty(MarkType)) markController.SetType(MarkType);
            }
            else
            {
                Debug.Log("Couldnt instantiate for some reason!");
                return null;
            }
        }
        MoveObjectsCircumference(number, positions, radius, center);
        return Objects.Cast<MarkController>().ToList();
    }

    public void SetHight(float height)
    {
        foreach (MarkController Mark in Objects)
        {
            Vector3 currentPosition = Mark.gameObject.transform.position;
            Mark.transform.position = new Vector3(currentPosition.x, height, currentPosition.z);
        }
    }
}
