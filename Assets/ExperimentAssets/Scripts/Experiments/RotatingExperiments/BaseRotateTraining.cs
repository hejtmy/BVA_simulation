﻿using System;
using BrainVR.UnityFramework.Crosshair;
using UnityEngine;
using Newtonsoft.Json;
using BrainVR.UnityFramework.DataHolders;
using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityFramework.Player;
using BrainVR.UnityFramework.Scripts.Objects.Goals;
using BrainVR.UnityFramework.InputControl;

namespace Assets.ExperimentAssets.Scripts.Experiments.RotatingExperiments
{
    public enum TrainingPhase
    {
        Instructions,
        Walking,
        Pointing,
        Room,
        Platform,
        All
    }
    public class BaseRotatesTraining : RotatingExperiment
    {
        public new BaseRotatesTrainingSettings Settings;
        private string _instructionText;
        public TrainingPhase TrainingPhase;
        private int _iCurrentGoal;
        public int ConsecutiveCorrect { get; set; }
        public GoalType CurrentTrialType;
        private float _trialStart;
        private float _trialFinish;

        //there is not everything because we implemet default versions from the parent classes
        protected override void OnExperimentInitialise()
        {
            base.OnExperimentInitialise();
            var currentSettings = SettingsHolder.Instance.CurrentExperimentSettings();
            Settings = (BaseRotatesTrainingSettings)currentSettings ?? new BaseRotatesTrainingSettings();
            ShouldLog = Settings.ShouldLog;
            CrosshairDefault();
        }
        protected override void OnExperimentSetup()
        {
            base.OnExperimentSetup();
            for (var i = 0; i < GoalManager.Goals.Count; i++)
            {
                GoalManager.GetGoal(i).SetColor(Settings.GoalColour(i));
            }
            //Moving and fixating goals and marks on specific arena positions
            GoalManager.MoveObjectsCircumference(Settings.NumberOfPositions, Settings.GoalPositions, Settings.GoalRadius);
            MarkManager.MoveObjectsCircumference(Settings.NumberOfPositions, Settings.MarksPositions, Settings.MarkRadius);
            ObjectFixations(GoalManager.Objects, Settings.GoalFixations);
            ObjectFixations(MarkManager.Objects, Settings.MarksFixations);

            //Training has fixerd rotation speed
            RotatePlatform(Settings.RotationSpeed[0]);
            CanvasManager.SetName("Trial number", "Správně:");
            TrainingPhase = TrainingPhase.Instructions;
            SetHelpText();
        }
        protected override void OnExperimentUpdate()
        {
            if (Input.GetButtonDown("ShowGoal")) SwitchGoal();
            if (Input.GetButtonDown("Confirm")) Confirmed();
            if (Input.GetButtonDown("StartTrial") && TrialState.Equals(TrialState.WaitingToStart)) TrialStart();
            if (Input.GetButtonDown("NextTrial") && TrialState.Equals(TrialState.Finished)) TrialSetNext();
            if (Input.GetButtonDown("ForceNextTrial")) ForceNextTrial();
        }
        protected override void AfterExperimentStart()
        {
            CanvasManager.SetText("Message", Settings.Message("Beginning"));
        }
        protected override void OnTrialSetup()
        {
            PlayerController.Instance.EnableMovement();
            if (TrainingPhase == TrainingPhase.Instructions) SetupInstructions();
            if (TrainingPhase == TrainingPhase.Walking) SetupWalking();
            if (TrainingPhase == TrainingPhase.Pointing) SetupPointing();
            if (TrainingPhase == TrainingPhase.Room) SetupRoom();
            if (TrainingPhase == TrainingPhase.Platform) SetupPlatform();
            if (TrainingPhase == TrainingPhase.All) SetupAll();
        }
        protected override void AfterTrialSetup()
        {
            CanvasManager.SetText("Instructions", _instructionText);
            SetHelpText();
            if (TrainingPhase == TrainingPhase.Walking)
            {
                TrialStart();
                return;
            }
            if (Center.PlayerInside) TrialStart();
            else
            {
                Center.OnEnter += CenterEntered;
                CanvasManager.SetText("Instructions", Settings.Message("NewTrial"));
            }
        }
        protected void SetHelpText()
        {
            if (TrainingPhase == TrainingPhase.Walking) CanvasManager.SetText("Help", Settings.Message("LearnWalking"));
            if (TrainingPhase == TrainingPhase.Pointing) CanvasManager.SetText("Help", Settings.Message("LearnPointing"));
            if (ConsecutiveCorrect >= 1 && TrainingPhase <= TrainingPhase.Pointing) CanvasManager.HideField("Help");
            if (TrainingPhase == TrainingPhase.Room) CanvasManager.SetText("Help", ConsecutiveCorrect >= Settings.HideGoalAfter ? 
                Settings.Message("TryRoom") : Settings.Message("LearnRoom"));
            if (TrainingPhase == TrainingPhase.Platform) CanvasManager.SetText("Help", ConsecutiveCorrect >= Settings.HideGoalAfter ? 
                Settings.Message("TryPlatform") : Settings.Message("LearnPlatform"));
            if (TrainingPhase == TrainingPhase.All) CanvasManager.SetText("Help", ConsecutiveCorrect >= Settings.HideGoalAfter ?
                Settings.Message("TryAll") : Settings.Message("LearnAll"));
            if (ConsecutiveCorrect >= 2) CanvasManager.HideField("Help");
        }
        protected override void OnTrialStart()
        {
            base.OnTrialStart();
            if (TrainingPhase == TrainingPhase.Instructions) StartInstructions();
            if (TrainingPhase == TrainingPhase.Walking) StartWalking();
            if (TrainingPhase == TrainingPhase.Pointing) StartPointing();
            if (TrainingPhase == TrainingPhase.Room) StartRoom();
            if (TrainingPhase == TrainingPhase.Platform) StartPlatform();
            if (TrainingPhase == TrainingPhase.All) StartAll();
            CanvasManager.SetText("Instructions", _instructionText);
        }
        protected override void AfterTrialStart()
        {
            _trialStart = Time.realtimeSinceStartup;
        }
        protected override void OnTrialFinished()
        {
            _trialFinish = Time.realtimeSinceStartup;
            base.OnTrialFinished();
            if (TrainingPhase >= TrainingPhase.Walking) WasOnTime();
            if (TrainingPhase == TrainingPhase.Instructions) FinishInstructions();
            if (TrainingPhase == TrainingPhase.Walking) FinishWalking();
            if (TrainingPhase == TrainingPhase.Pointing) FinishPointing();
            if (TrainingPhase >= TrainingPhase.Platform) FinishAll();
            BeepManager.Play("Finished");
            CanvasManager.SetText("Instructions", "Dokončeno.");
            CanvasManager.SetText("Trial number", ConsecutiveCorrect.ToString());
        }
        protected override void AfterTrialFinished()
        {
            TrialSetNext();
            if(TrainingPhase >= TrainingPhase.Room) CanvasManager.SetText("Instructions", Settings.Message("NewTrial"));
        }
        protected override void OnTrialClosed()
        {
            PhaseProgression();
            if (CurrentGoal()) CurrentGoal().Show(false);
        }
        protected override void AfterTrialClosed()
        {
        }
        protected override void OnExperimentClosed()
        {
            CanvasManager.SetText("Instructions", Settings.Message("CloseExperiment"));
        }
        protected override bool CheckForEnd()
        {
            return TrainingPhase > TrainingPhase.All;
        }
        #region Flow functions

        #region TrialSetups
        private void SetupInstructions()
        {
            PlayerController.Instance.MoveToCenter();
            PlayerController.Instance.EnableMovement(false);
        }
        private void SetupWalking()
        {
            _instructionText = Settings.Message("GoToGoal");
            RandomizeGoal();
            CurrentGoal().OnEnter += GoalEnter;
        }
        private void SetupPointing()
        {
            _instructionText = Settings.Message("GoToCenter");
            RandomizeGoal();
        }
        private void SetupPlatform()
        {
            _instructionText = Settings.Message("GoToCenter");
            RandomizeGoal(GoalType.Platform);
        }
        private void SetupRoom()
        {
            _instructionText = Settings.Message("GoToCenter");
            RandomizeGoal(GoalType.Room);
        }
        private void SetupAll()
        {
            _instructionText = Settings.Message("GoToCenter");
            RandomizeGoal();
        }
        #endregion
        #region TrialStarts
        private void StartInstructions()
        {
            _instructionText = Settings.Message("Confirm");
            switch (ConsecutiveCorrect)
            {
                case 0:
                    CanvasManager.SetText("Help", Settings.Message("DescribeUI"));
                    break;
                case 1:
                    CanvasManager.SetText("Help", Settings.Message("DescribeRoom"));
                    break;
                case 2:
                    CanvasManager.SetText("Help", Settings.Message("DescribeGoals"));
                    GoalManager.Show(0);
                    GoalManager.Show(3);
                    break;
                case 3:
                    CanvasManager.SetText("Help", Settings.Message("DescribeTraining"));
                    GoalManager.ShowAll();
                    break;
                default:
                    break;
            }
        } 
        private void StartWalking()
        {
            _instructionText = Settings.Message("GoToGoal");
            CurrentGoal().Show();
            PlayerController.Instance.EnableMovement();
        }
        private void StartPointing()
        {
            _instructionText = Settings.Message("Point");
            if (ConsecutiveCorrect < Settings.HideGoalAfter) CurrentGoal().Show();
            if (ConsecutiveCorrect < Settings.HideGoalAfter) CanvasManager.SetText("Help", Settings.Message("LearnPointing"));
            Pointing();
            CurrentGoal().Show();
        }
        private void StartPlatform()
        {
            StartGoalTraining(Settings.Message("LearnPlatform"));
        }
        private void StartRoom()
        {
            StartGoalTraining(Settings.Message("LearnRoom"));
        }
        private void StartAll()
        {
            StartGoalTraining(Settings.Message("LearnAll"));
        }
        private void StartGoalTraining(string message)
        {
            _instructionText = Settings.Message("Point");
            if (ConsecutiveCorrect < Settings.HideGoalAfter) CanvasManager.SetText("Help", message);
            Pointing();
            if (ConsecutiveCorrect < Settings.HideGoalAfter) CurrentGoal().Show();
        }
        private void Pointing()
        {
            CrosshairPointing();
            PlayerController.Instance.EnableMovement(false);
            InputManager.PointButtonPressed += Point;
            _instructionText = Settings.Message("Point");
        }
        #endregion
        #region TrialFinishes
        private void FinishInstructions()
        {
            ConsecutiveCorrect++;
            GoalManager.ShowAll(false);
            if (ConsecutiveCorrect >= 4) PhaseProgression(true);
        }
        private void FinishWalking()
        {
            CurrentGoal().OnEnter -= GoalEnter;
        }
        private void FinishPointing()
        {
            InputManager.PointButtonPressed -= Point;
            PlayerController.Instance.EnableMovement();
        }
        private void FinishAll()
        {
            CurrentGoal().OnEnter -= GoalEnter;
        }
        private void WasOnTime()
        {
            var diff = _trialFinish - _trialStart;
            if (diff < Settings.TrialTimeLimit)
            {
                CanvasManager.SetText("Message", "Výborně, stihli jste to v časovém limitu");
                ConsecutiveCorrect++;
            }
            else
            {
                CanvasManager.SetText("Message",
                    "Jste pomalejší o " + Math.Round(diff - Settings.TrialTimeLimit, 1) +
                    " s. Zkuste to trochu rychleji.");
                ConsecutiveCorrect = 0;
            }
        }
        #endregion
        private void Confirmed()
        {
            switch (TrainingPhase)
            {
                case TrainingPhase.Instructions:
                    TrialFinish();
                    break;
                default:
                    break;
            }
        }
        private void ForcePhaseProgression()
        {
            //finalise and close the trial?
            // progress the phase
            // make sure it doesn't progress again
        }
        private void PhaseProgression(bool force = false)
        {
            if (!force)
            {
                if (TrainingPhase == TrainingPhase.Instructions) return;  //instructions can be as long as necessary
                if (ConsecutiveCorrect < Settings.CorrectTrialsToAdvance) return;
            }
            TrainingPhase++;
            ConsecutiveCorrect = 0;
            CanvasManager.SetText("Trial number", "");
            Debug.Log("Phase progressed to " + TrainingPhase);
        }

        private bool CheckInstructionsFinished()
        {
            return ConsecutiveCorrect >= 4;
        }

        private void Point()
        {
            if (!CorrectFacing(CurrentGoal().gameObject, 20))
            {
                CanvasManager.SetText("Message", Settings.Message("PointWrong"));
                ConsecutiveCorrect = -1; //will get bumped by 1 or set to zero on finish
                CurrentGoal().Show();
                return;
            }
            CanvasManager.SetText("Message", Settings.Message("PointWell"));
            CrosshairDefault();
            if (TrainingPhase >= TrainingPhase.Room) ContinueTrial();
            if (TrainingPhase == TrainingPhase.Pointing) TrialFinish();
        }
        public void CenterEntered(GoalController sender, EventArgs e)
        {
            if (TrialState != TrialState.WaitingToStart) return;
            Center.OnEnter -= CenterEntered;
            TrialStart();
        }
        private bool CorrectFacing(GameObject target, float allowedDifference)
        {
            var leveledTarget = new Vector3(target.transform.position.x, 0, target.transform.position.z);
            var leveledPlayer = new Vector3(PlayerController.Instance.transform.position.x, 0, PlayerController.Instance.transform.position.z);
            return !(Vector3.Angle(leveledTarget - leveledPlayer, PlayerController.Instance.transform.forward) > allowedDifference);
        }
        private void ContinueTrial()
        {
            PlayerController.Instance.EnableMovement();
            CurrentGoal().OnEnter += GoalEntered;
            CanvasManager.SetText("Instructions", Settings.Message("GoToGoal"));
        }
        public void GoalEntered(GoalController sender, EventArgs e)
        {
            if (TrialState != TrialState.Running) return;
            CurrentGoal().OnEnter -= GoalEntered;
            TrialFinish();
        }
        public override string ExperimentHeaderLog()
        {
            //deserialise 
            var str = "***EXPERIMENT SETTINGS***" + Environment.NewLine;
            str += Settings.SerialiseSettings();
            str += Environment.NewLine + "---EXPERIMENT SETTINGS---" + Environment.NewLine;
            str += "***POSITIONS***" + Environment.NewLine;
            var header = new TestHeader();
            foreach (var goal in GoalManager.Goals)
            {
                header.GoalPositions.Add(goal.transform.position.ToString());
            }
            str += JsonConvert.SerializeObject(header, Formatting.Indented);
            str += Environment.NewLine + "---POSITIONS---" + Environment.NewLine;
            return str;
        }
        #endregion
        #region Experiment Functions
        // CUSTOM FUNCTIONS FOR SPECIFIC EXPERIMENT
        private void RandomizeGoal(GoalType type = GoalType.Both)
        {
            var lowerBound = type == GoalType.Platform ? 2 : 0;
            var upperBound = type == GoalType.Room ? 1 : 3;
            var iNewGoal = UnityEngine.Random.Range(lowerBound, upperBound + 1);
            if (iNewGoal == _iCurrentGoal) RandomizeGoal(type);
            else _iCurrentGoal = iNewGoal;
        }
        private GoalController CurrentGoal()
        {
            return GoalManager.GetGoal(_iCurrentGoal);
        }
        private void GoalEnter(GoalController sender, EventArgs e)
        {
            if (TrialState == TrialState.Running) TrialFinish();
        }
        private void SwitchGoal()
        {
            string message = CurrentGoal().Switch() ? "GoalDisplayed" : "GoalHidden";
            SendTrialEvent(message);
        }
        private void CrosshairPointing()
        {
            CrosshairController.Instance.SetColour(Settings.GoalColour(_iCurrentGoal));
            CrosshairController.Instance.SetImage("circle-dot");
            CrosshairController.Instance.Resize(new Vector2(100, 100));
        }
        private void CrosshairDefault()
        {
            CrosshairController.Instance.SetImage("dot");
            CrosshairController.Instance.ResetColor();
            CrosshairController.Instance.Resize(new Vector2(5, 5));
        }

        public override void AddSettings(ExperimentSettings settings)
        {
            Settings = (settings != null ? (BaseRotatesTrainingSettings)settings : null) ?? new BaseRotatesTrainingSettings();
        }
        #endregion
    }
}