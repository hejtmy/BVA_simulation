﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using BrainVR.UnityFramework.Crosshair;
using Newtonsoft.Json;
using BrainVR.UnityFramework.Player;
using BrainVR.UnityFramework.DataHolders;
using BrainVR.UnityFramework.Objects.Goals;
using BrainVR.UnityFramework.Scripts.Objects.Goals;
using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityLogger;

namespace Assets.ExperimentAssets.Scripts.Experiments.RotatingExperiments
{
    public class TestHeader
    {
        public List<string> GoalPositions = new List<string>();
    }
    public class BaseRotates : RotatingExperiment
    {
        public new BaseRotatesSettings Settings;
        private BaseRotatesResults _results;

        private bool _alreadyPointed;
        private bool _finishedInTime;

        private IEnumerator _waitForPulse;

        //there is not everything because we implemet default versions from the parent classes
        protected override void OnExperimentInitialise()
        {
            base.OnExperimentInitialise();
            AddSettings(SettingsHolder.Instance.CurrentExperimentSettings());
            ShouldLog = Settings.ShouldLog;
            CrosshairDefault();
            _results = new BaseRotatesResults(ExperimentInfo.Instance.Participant.Id);
        }
        protected override void OnExperimentSetup()
        {
            base.OnExperimentSetup();
            for (var i = 0; i < GoalManager.Goals.Count; i++)
            {
                GoalManager.GetGoal(i).SetColor(Settings.GoalColour(i));
            }
            //Moving and fixating goals and marks on specific arena positions
            GoalManager.MoveObjectsCircumference(Settings.NumberOfPositions, Settings.GoalPositions, Settings.GoalRadius);
            MarkManager.MoveObjectsCircumference(Settings.NumberOfPositions, Settings.MarksPositions, Settings.MarkRadius);
            ObjectFixations(GoalManager.Objects, Settings.GoalFixations);
            ObjectFixations(MarkManager.Objects, Settings.MarksFixations);
        }
        protected override void OnExperimentUpdate()
        {
            if (CheckTrialTimeLimit()) TrialTimeout();
            if (Input.GetButtonDown("ShowGoal")) SwitchGoal();
            if (Input.GetButtonDown("StartTrial") && TrialState.Equals(TrialState.WaitingToStart)) TrialStart();
            if (Input.GetButtonDown("NextTrial") && TrialState.Equals(TrialState.Finished)) TrialSetNext();
            if (Input.GetButtonDown("ForceNextTrial")) ForceNextTrial();
            if (Input.GetButtonDown("Point")) Pointed();
        }
        protected override void OnTrialSetup()
        {
            PlayerController.Instance.EnableMovement();
            SetRotationSpeed();
            _results.GoalTypes.Add(GoalType());
            //It could have been the same from forced finished trial
            CrosshairDefault();
            CrosshairController.Instance.ResetColor();
        }
        protected override void AfterTrialSetup()
        {
            if (Center.PlayerInside) PrivateStart();
            else
            {
                Center.OnEnter += CenterEntered;
                CanvasManager.SetText("Instructions", Settings.Message("NewTrial"));
            }
        }
        //gets called before the actuial trial start
        private void PrivateStart()
        {
            PlayerController.Instance.EnableMovement(false);
            if (Settings.WaitForPulse) StartCoroutine(StartAfterPulse());
            else TrialStart();
        }
        protected override void OnTrialStart()
        {
            base.OnTrialStart();
            if (Settings.TrainingLength > TrialNumber) CurrentGoal.Show();
            StartPointing();
            _alreadyPointed = false;
            _finishedInTime = true;
        }
        private IEnumerator StartAfterPulse()
        {
            var currentTrial = TrialNumber;
            while (!Input.GetButtonDown("fMRISynchro")) yield return null;
            //safety check we didn't change the trial number
            if(currentTrial == TrialNumber) TrialStart();
        }
        protected override void OnTrialFinished()
        {
            _results.TrialTimes.Add(Time.realtimeSinceStartup - TrialStartTime);
            _results.FinishedInTime.Add(_finishedInTime);
            CurrentGoal.Hide();
            CanvasManager.SetText("Trial number", (TrialNumber + 1).ToString());
        }
        protected override void AfterTrialFinished()
        {
            TrialSetNext();
        }
        protected override void OnExperimentClosed()
        {
            CanvasManager.SetText("Instructions", Settings.Message("CloseExperiment"));
            PlayerController.Instance.EnableMovement();
            _results.Save();
        }
        protected override bool CheckForEnd()
        {
            return TrialNumber >= Settings.GoalOrder.Length - 1;
        }
        #region Flow functions
        public void CenterEntered(GoalController sender, EventArgs e)
        {
            Debug.Log("Center entered");
            if (TrialState != TrialState.WaitingToStart) return;
            Center.OnEnter -= CenterEntered;
            PlayerController.Instance.EnableMovement(false);
            PrivateStart();
        }
        private float FacingAngleDifference(GameObject target)
        {
            var leveledTarget = new Vector3(target.transform.position.x, 0, target.transform.position.z);
            var leveledPlayer = new Vector3(PlayerController.Instance.transform.position.x, 0, PlayerController.Instance.transform.position.z);
            return Vector3.Angle(leveledTarget - leveledPlayer, PlayerController.Instance.transform.forward);
        }
        private void StartPointing()
        {
            CanvasManager.SetText("Instructions", Settings.Message("Point"));
            PlayerController.Instance.EnableMovement(false);
            CrosshairPointing();
        }
        private void Pointed()
        {
            if (TrialState != TrialState.Running || _alreadyPointed) return;
            _alreadyPointed = true;
            //results
            var angleError = FacingAngleDifference(CurrentGoal.gameObject);
            var trialTime = Time.realtimeSinceStartup - TrialStartTime;
            NotePointingResults(trialTime, angleError);
            //continuing trial
            CrosshairDefault();
            if (Settings.GoToGoal) ContinueTrial();
            else TrialFinish();
        }
        private void NotePointingResults(float time, float angleError)
        {
            _results.PointTimes.Add(time);
            _results.AngleError.Add(angleError);
            MarkCorrect(angleError < Settings.AcceptedPointingError);
        }
        private void ContinueTrial()
        {
            PlayerController.Instance.EnableMovement();
            CurrentGoal.OnEnter += GoalEntered;
            CanvasManager.SetText("Instructions", Settings.Message("GoToGoal"));
        }
        public void GoalEntered(GoalController sender, EventArgs e)
        {
            if (TrialState != TrialState.Running) return;
            CurrentGoal.OnEnter -= GoalEntered;
            TrialFinish();
        }
        public override string ExperimentHeaderLog()
        {
            //deserialise 
            var str = "***EXPERIMENT SETTINGS***" + Environment.NewLine;
            str += Settings.SerialiseSettings();
            str += Environment.NewLine + "---EXPERIMENT SETTINGS---" + Environment.NewLine;
            str += "***POSITIONS***" + Environment.NewLine;
            var header = new TestHeader();
            foreach (var goal in GoalManager.Goals)
            {
                header.GoalPositions.Add(goal.transform.position.ToString());
            }
            str += JsonConvert.SerializeObject(header, Formatting.Indented);
            str += Environment.NewLine + "---POSITIONS---" + Environment.NewLine;
            return str;
        }
        private bool CheckTrialTimeLimit()
        {
            if (TrialState != TrialState.Running) return false;
            return (Settings.TrialTimeLimit > 0) && ((Time.realtimeSinceStartup - TrialStartTime) > Settings.TrialTimeLimit);
        }
        private void TrialTimeout()
        {
            //what needs to happen
            //results
            if (!_alreadyPointed) NotePointingResults(0, 999);
            _finishedInTime = false;
            TrialFinish();
        }
        #endregion
        #region Experiment Functions
        // CUSTOM FUNCTIONS FOR SPECIFIC EXPERIMENT
        private int CurrentGoalIndex { get { return Settings.GoalOrder[TrialNumber]; } }
        private GoalController CurrentGoal { get { return GoalManager.GetGoal(CurrentGoalIndex); } }
        private void SwitchGoal()
        {
            string message = CurrentGoal.Switch() ? "GoalDisplayed" : "GoalHidden";
            SendTrialEvent(message);
        }
        private void CrosshairPointing()
        {
            CrosshairController.Instance.SetImage("circle-dot");
            CrosshairController.Instance.Resize(new Vector2(100, 100));
            CrosshairController.Instance.SetColour(Settings.GoalColour(CurrentGoalIndex));
        }
        private void CrosshairDefault()
        {
            CrosshairController.Instance.SetImage("dot");
            CrosshairController.Instance.Resize(new Vector2(5, 5));
            CrosshairController.Instance.ResetColor();
        }
        private void MarkCorrect(bool correct)
        {
            if (_results.CorrectPointing.Count <= TrialNumber) _results.CorrectPointing.Add(correct);
        }
        private GoalType GoalType()
        {
            return (CurrentGoalIndex > 1) ? RotatingExperiments.GoalType.Room : RotatingExperiments.GoalType.Platform;
        }
        private void SetRotationSpeed()
        {
            RotatePlatform(TrialNumber < Settings.RotationSpeed.Length ? Settings.RotationSpeed[TrialNumber] : Settings.RotationSpeed.Last());
        }
        public override void AddSettings(ExperimentSettings settings)
        {
            if (settings == null)
            {
                Settings = new BaseRotatesSettings();
            }
            else
            {
                Settings = (BaseRotatesSettings)settings;
            }
        }
        #endregion
    }
}