﻿using Assets.ExperimentAssets.Scripts.Experiments.RotatingExperiments;
using BrainVR.UnityLogger;
using System.Collections.Generic;

namespace Assets.ExperimentAssets.Scripts.Experiments
{
    public class BaseRotatesResults : ResultsLog
    {
        public BaseRotatesResults(string participantId) : base(participantId)
        {
            ExperimentName = "BaseRotates";
        }

        [TestData] public List<float> PointTimes = new List<float>();
        [TestData] public List<float> AngleError = new List<float>();
        [TestData] public List<float> TrialTimes = new List<float>();
        [TestData] public List<bool> CorrectPointing = new List<bool>();
        [TestData] public List<GoalType> GoalTypes = new List<GoalType>();
        [TestData] public List<bool> FinishedInTime = new List<bool>();

        [ResultData] public float AveragePointTime { get { return Average(PointTimes, FinishedInTime.FindAllIndexof(true)); } }
        [ResultData] public float AverageError { get { return Average(AngleError, FinishedInTime.FindAllIndexof(true)); } }
        [ResultData] public float AverageTrialTime { get { return Average(TrialTimes, FinishedInTime.FindAllIndexof(true)); } }
        [ResultData] public int CorrectPoints { get { return SumBool(CorrectPointing, FinishedInTime.FindAllIndexof(true)); } }
        [ResultData] public int FinishedTrials { get { return SumBool(FinishedInTime); } }
        [ResultData] public float AveragePointTimeRoom { get { return Average(PointTimes, new List<int[]> { FinishedInTime.FindAllIndexof(true), GoalTypes.FindAllIndexof(GoalType.Room)}); } }
        [ResultData] public float AveragePointTimePlatform { get { return Average(PointTimes, new List<int[]>{ FinishedInTime.FindAllIndexof(true), GoalTypes.FindAllIndexof(GoalType.Platform)}); } }
        [ResultData] public float AverageErrorRoom { get { return Average(AngleError, new List<int[]> { FinishedInTime.FindAllIndexof(true), GoalTypes.FindAllIndexof(GoalType.Room)}); } }
        [ResultData] public float AverageErrorPlatform { get { return Average(AngleError, new List<int[]> { FinishedInTime.FindAllIndexof(true), GoalTypes.FindAllIndexof(GoalType.Platform)}); } }
        [ResultData] public int CorrectPointRoom { get { return SumBool(CorrectPointing, new List<int[]> { FinishedInTime.FindAllIndexof(true), GoalTypes.FindAllIndexof(GoalType.Room)}); } }
        [ResultData] public int CorrectPointPlatform { get { return SumBool(CorrectPointing, new List<int[]> { FinishedInTime.FindAllIndexof(true), GoalTypes.FindAllIndexof(GoalType.Platform)}); } }

        protected override string FormatData()
        {
            return CustomData;
        }
    }
}
