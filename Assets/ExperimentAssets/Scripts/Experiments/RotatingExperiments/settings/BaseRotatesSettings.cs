﻿using System.Collections.Generic;
using UnityEngine;
using BrainVR.UnityFramework.Helpers;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Assets.ExperimentAssets.Scripts.Experiments.RotatingExperiments
{
    public class BaseRotatesSettings : RotatingSettings
    {
        public float TrialTimeLimit = 10f;
        public bool WaitForPulse = false;

        public BaseRotatesSettings()
        {
            Messages = new Dictionary<string, string>
            {
                {"Point", "Ukažte na cíl"},
                {"PointWrong", "Ukázali jste nepřesně"},
                {"GoToGoal", "Dojděte do cíle"},
                {"NewTrial", "Nový úkol můžete začít ze středu"},
                {"Closed Experiment", "Děkujeme za absolvování experimentu"}
            };
        }

#if UNITY_EDITOR
        [MenuItem("Assets/Experiment/BaseRotatesSettings")]
        public static void CreateDialogueLine()
        {
            ScriptableObjectUtility.CreateAsset<BaseRotatesSettings>();
        }
        [CustomEditor(typeof(BaseRotatesSettings))]
        public class SettingsEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();
                BaseRotatesSettings myScript = (BaseRotatesSettings)target;
                if (GUILayout.Button("Serialise settings"))
                {
                    Debug.Log(myScript.SerialiseOut());
                }
            }
        }

#endif
    }
}   
