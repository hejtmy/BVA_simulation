﻿using BrainVR.UnityFramework.Experiment;
using UnityEngine;

namespace Assets.ExperimentAssets.Scripts.Experiments.RotatingExperiments
{
    public class RotatingSettings : ExperimentSettings
    {

        protected float ArenaSize = 30;
        public float GoalSize = 5f;
        public int[] GoalOrder = { 0, 1, 2, 3, 0, 1, 2, 3 };
        public float[] RotationSpeed = {5};
        public string[] GoalColours = { "#8b0000", "#ffd700", "#105955", "#104e8b", "#000000", "#ffffff" };
        public int TrainingLength = 2;
        public float GoalRadius = 22;
        public float MarkRadius = 25;
        public int NumberOfPositions = 12;
        public int[] MarksPositions = {0, 4, 8, 2, 6, 10 };
        public string[] MarksFixations = { "room", "room", "room", "platform", "platform", "platform" };
        public int[] GoalPositions = {1, 3, 9, 11};
        public string[] GoalFixations = { "room", "room", "platform", "platform" };
        public bool PointAtFirst = true;
        public bool GoToGoal = true;
        public float AcceptedPointingError = 20f;
        public Color GoalColour(int i)
        {
            Color myColor;
            ColorUtility.TryParseHtmlString(GoalColours[i], out myColor);
            return myColor;
        }
    }
}
