﻿using System.Collections.Generic;
using Assets.GeneralScripts;
using UnityEngine;
using BrainVR.UnityFramework.Helpers;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Assets.ExperimentAssets.Scripts.Experiments.RotatingExperiments
{
    public class BaseRotatesTrainingSettings : RotatingSettings
    {
        public int HideGoalAfter = 2;
        public int CorrectTrialsToAdvance = 5;
        public float TrialTimeLimit = 10f;

        public BaseRotatesTrainingSettings()
        {
            Messages = new Dictionary<string, string>
            {
                {"Confirm", "Pro pokračování stiskněte mezerník" },
                {"Beginning", "V tomto experimentu se budou dít věci"},
                {"PointWrong", "Ukázali jste nepřesně"},
                {"PointWell", "Ukázali jste správně" },
                {"GoToCenter", "Dojděte do středu"},
                {"Point", "Ukažte na cíl" },
                {"GoToGoal", "Dojděte do cíle"},
                {"NewTrial", "Nový úkol můžete začít ze středu"},
                {"Closed Experiment", "Děkujeme za absolvování experimentu"},
                {"DescribeUI", "V levém horníám rohu máte informace o počtu absolvovaných úkolů. V pravo nahoře pak zadání součaného úkolu." },
                {"DescribeRoom", "Aréna pod vámi se během experimentu bude otáčet. Všimněte si, že některé stromy stojí na aréně a některé mimo ni." },
                {"DescribeGoals", "Cíle v aréně jsou buď přilepené na   arénu, nebo na ní nezávislé. Všimněte si dvou cílů. Jeden z nich see otáčí, druhý nikoli." },
                {"DescribeTraining", "Během tréninku se naučíte pozice 4 cílů Dva z nich přilepené na arénu, dva stacionární." },
                {"LearnWalking", "Chůzi ovládáte tlačítky W A S D nebo směrovými šipkami. Projděte se po viditelných cílech." },
                {"LearnPointing", "Když se Vám zobrazí kruh ve středu obrazovky, je třeba ukázat na cíl. Ukzovat můžete pouze ze středu arény."},
                {"LearnRoom", "Nyní je cíl vázaný na arénu. Nejprve na něj ukažte ze středu, poté dojděte an patřičné místo" },
                {"TryRoom", "Cíl již není vidět, Zkuste jej najít sami. Opět nejprve ukázat ze středu, poté dojít na správné místo."},
                {"LearnPlatform", "Nyní je cíl přilepený na rotující platformu. Nejprve na něj ukažte ze středu, poté dojděte na patřičné místo"},
                {"TryPlatform", "Cíl již není vidět, Zkuste jej najít sami. Opět nejprve ukázat ze středu, poté dojít na správné místo."},
                {"LearnAll", "Teď již znáte pozice všech cílů. Budou se střídat cíle na aréně i na platformě." }
            };
        }

#if UNITY_EDITOR
        [MenuItem("Assets/Experiment/BaseRotatesTrainingSettings")]
        public static void CreateDialogueLine()
        {
            ScriptableObjectUtility.CreateAsset<BaseRotatesTrainingSettings>();
        }
        [CustomEditor(typeof(BaseRotatesTrainingSettings))]
        public class SettingsEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();
                BaseRotatesTrainingSettings myScript = (BaseRotatesTrainingSettings)target;
                if (GUILayout.Button("Serialise settings"))
                {
                    Debug.Log(myScript.SerialiseOut());
                }
            }
        }

#endif
    }
}
