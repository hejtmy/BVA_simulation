﻿using System.Collections.Generic;
using UnityEngine;
using BrainVR.UnityFramework.Arduino;
using BrainVR.UnityFramework.Objects.Marks;
using BrainVR.UnityFramework.Objects.Goals;
using BrainVR.UnityFramework.Scripts.Objects.Beeper;
using BrainVR.UnityFramework.Scripts.Objects.Goals;
using BrainVR.UnityFramework.Objects;
using BrainVR.UnityFramework.Scripts.Objects.Marks;
using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityFramework.UI.InGame;

namespace Assets.ExperimentAssets.Scripts.Experiments.RotatingExperiments
{
    public enum GoalType
    {
        Both,
        Room,
        Platform
    }
    public abstract class RotatingExperiment : BaseExperiment
    {
        protected float ArenaSize = 20;

        //maybe more to some other abstract class
        protected MarkManager MarkManager;
        protected GoalManager GoalManager;
        protected BeeperManager BeepManager;
        protected GoalController Center;
        protected ArenaObject Platform;
        protected ExperimentCanvasManager CanvasManager;
        protected ArduinoController Arduino;
        protected MarkController CurrentMark;

        protected float TrialStartTime;
        protected float TrialEndTime;
        protected List<float> TrialTimes = new List<float>();

        #region Experiment Logic
        protected override void OnExperimentInitialise()
        {
            GoalManager = GoalManager.Instance;
            MarkManager = MarkManager.Instance;
            CanvasManager = ExperimentCanvasManager.Instance;
        }
        protected override void OnExperimentSetup()
        {
            BeepManager = BeeperManager.Instance;
            Platform = GameObject.Find("Platform").GetComponent<ArenaObject>();
            Center = GameObject.Find("Center").GetComponent<GoalController>();
            GoalManager.ShowAll(false);
            CanvasManager.Show();
        }
        protected override void OnExperimentStart()
        {
            TrialNumber = 0;
        }
        protected override void OnTrialStart()
        {
            //wait until synchropulse is sent
            TrialStartTime = Time.realtimeSinceStartup;
        }
        protected override void OnTrialFinished()
        {
            TrialEndTime = Time.realtimeSinceStartup;
            TrialTimes.Add(TrialEndTime - TrialStartTime);
        }
        protected override void OnExperimentClosed()
        {
            Destroy(GoalManager.gameObject);
        }
        protected override void AfterExperimentClosed()
        {
            CanvasManager.Show(false);
        }
        #endregion
        #region Helpers
        protected void RotatePlatform(float speed)
        {
            Platform.StartRotation(Vector3.up, speed);
        }

        protected void ObjectFixations(List<ArenaObject> objects, string[] fixations)
        {
            //validations
            if (objects.Count != fixations.Length)
            {
                Debug.Log("These are not equal long arrays, cannot perform proper fixations");
                return;
            }
            //check for correct spellings
            for(var i = 0; i < objects.Count; i++)
            {
                if (fixations[i] == "platform") objects[i].gameObject.transform.SetParent(Platform.transform);
            }
        }
        #endregion
    }
}
