﻿using BrainVR.UnityFramework.Arduino;
using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityFramework.InputControl;
using BrainVR.UnityFramework.Player;
using BrainVR.UnityFramework.UI.InGame;
using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Assets.ExperimentAssets.Scripts.Experiments.WalkingCircle
{
    public class WalkingExperiment : BaseExperiment
    {
        public new WalkingExperimentSettings Settings;

        private PlayerController _player;
        private ArduinoController _arduino;

        public Transform Character;
        public enum Direction { Forward, Reverse };

        private float _pathPosition;
        private float _pathTraveled;

        #region ExperimentFlow
        protected override void OnExperimentInitialise()
        {
            _player = PlayerController.Instance;
            _arduino = ArduinoController.Instance;
        }
        protected override void OnExperimentSetup()
        {
            //Connects the arduino
            _arduino.Connect();
        }
        protected override void OnExperimentStart()
        {
            //diable player movement and rotation
            //_player.EnableRotation(false);
            _player.EnableMovement(false);
            //nullify all rotations but y since we just want to look where we are going:
            _player.Rotation = new Vector2(10, 0);
            _player.Vector2Position = CirclePoint(Settings.Circumference, 0);
        }
        protected override void OnExperimentFinished()
        {
            ExperimentCanvasManager.Instance.Show();
            ExperimentCanvasManager.Instance.SetText("Instructions", "");
            ExperimentCanvasManager.Instance.SetText("Help", Settings.Message("end"));
        }
        protected override void OnExperimentUpdate()
        {
            if(TrialState == TrialState.Running)
            {
                if (Input.GetKey("up")) UpdatePlayer();
                if (CrossPlatformInputManager.GetAxis("Vertical") > 0) UpdatePlayer(); 
            }

        }
        protected override void AfterTrialFinished()
        {
            NextTrial();
        }
        protected override void OnTrialSetup()
        {
            //subscribe to confirm
            InputManager.ConfirmationButtonPressed += StartTrial;
            ExperimentCanvasManager.Instance.Show();
            ExperimentCanvasManager.Instance.SetText("Instructions", Settings.Message("confirm"));
        }
        protected override void OnTrialStart()
        {
            //unsubscribe confirmation
            ExperimentCanvasManager.Instance.Show(false);
            InputManager.ConfirmationButtonPressed -= StartTrial;
            if (_arduino.SendPulsUp()) SendTrialEvent("ArduinoPulseUp");

        }
        protected override void OnTrialFinished()
        {
            if (_arduino.SendPulsDown()) SendTrialEvent("ArduinoPulseDown");
        }
        #endregion
        #region Experiemnt functions
        private float CurrentSpeed
        {
            get { return Settings.Speeds[TrialNumber]/100; }
        }
        private Direction CurrectDirection
        {
            get { return (Direction)Settings.Direction[TrialNumber]; }
        }
        public object CanvasManager { get; private set; }
        private void UpdatePlayer()
        {
            CheckForTrialEnd();
            ChangePathPosition();
            SetPlayerPosition();
            if (Settings.SetRotation) SetPlayerRotation();
        }
        private void ChangePathPosition()
        {
            switch (CurrectDirection)
            {
                case Direction.Forward:
                    _pathPosition += Time.deltaTime * CurrentSpeed;
                    break;
                case Direction.Reverse:
                    //handle path loop around since we can't interpolate a path percentage that's negative(well duh):
                    var temp = _pathPosition - (Time.deltaTime * CurrentSpeed);
                    _pathPosition = (temp < 0) ? 2 : _pathPosition - (Time.deltaTime * CurrentSpeed); ;
                    break;
            }
            _pathTraveled += Time.deltaTime * CurrentSpeed;
        }
        void SetPlayerPosition()
        {
            var coordinateOnPath = CirclePoint(Settings.Circumference, _pathPosition);
            _player.Vector2Position = new Vector2(coordinateOnPath.x, coordinateOnPath.y);
        }
        void SetPlayerRotation()
        {
            var lookTarget = CurrectDirection == Direction.Forward ? CirclePoint(Settings.Circumference, _pathPosition + Settings.LookAheadAmount) :
               CirclePoint(Settings.Circumference, _pathPosition - Settings.LookAheadAmount);
            //moves the loking target a bit to the center
            _player.LookAtPosition(new Vector2(lookTarget.x, lookTarget.y));
        }
        void CheckForTrialEnd()
        {
            //changes every 2
            if (_pathTraveled > 2*(TrialNumber+1)) FinishTrial();
        }
        private static Vector2 CirclePoint(float radius, float percent)
        {
            var angle = percent * Mathf.PI;
            var position = new Vector2(radius * Mathf.Cos(angle), radius * Mathf.Sin(angle));
            return position;
        }
        #endregion
        #region MonoBehaviour
        #endregion
        public override string ExperimentHeaderLog()
        {
            //deserialise 
            var str = "***EXPERIMENT SETTINGS***" + Environment.NewLine;
            str += Settings.SerialiseSettings();
            str += Environment.NewLine + "---EXPERIMENT SETTINGS---" + Environment.NewLine;
            return str;
        }
        protected override bool CheckForEnd()
        {
            return TrialNumber >= Settings.Speeds.Length - 1;
        }
        public override void AddSettings(ExperimentSettings settings)
        {
            Settings = (settings != null ? (WalkingExperimentSettings)settings : null) ?? new WalkingExperimentSettings();
        }
    }
}
