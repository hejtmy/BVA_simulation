﻿using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityFramework.Helpers;
using UnityEditor;
using UnityEngine;

namespace Assets.ExperimentAssets.Scripts.Experiments.WalkingCircle
{
    public class WalkingExperimentSettings : ExperimentSettings
    {
        public float Circumference = 10f;
        public float[] Speeds = { 10f, 5f, 10f };
        public int[] Direction = { 0, 1, 0 };
        public float Height = 5f;
        public bool SetRotation = false;
        public float LookAheadAmount = 0.3f;

        public WalkingExperimentSettings()
        {
            Messages = new System.Collections.Generic.Dictionary<string, string>
            {
                { "confirm", "Pro započetí stiskněte mezerník."},
                {"end", "Děkujeme za absolvování experimentu." }
            };
        }

#if UNITY_EDITOR
        [MenuItem("Assets/Experiment/Walking Experiment Settings")]
        public static void CreateDialogueLine()
        {
            ScriptableObjectUtility.CreateAsset<WalkingExperimentSettings>();
        }
        [CustomEditor(typeof(WalkingExperimentSettings))]
        public class SettingsEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();
                var myScript = (WalkingExperimentSettings)target;
                if (GUILayout.Button("Serialise settings")) Debug.Log(myScript.SerialiseOut());
            }
        }
#endif
    }
}
