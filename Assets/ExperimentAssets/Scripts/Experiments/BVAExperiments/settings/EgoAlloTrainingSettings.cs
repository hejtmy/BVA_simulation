﻿using System.Collections.Generic;
using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityFramework.Helpers;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Assets.ExperimentAssets.Scripts.Experiments.BVAExperiments
{
    public class EgoAlloTrainingSettings : ExperimentSettings
    {
        protected float ArenaSize = 20;
        public float GoalSize = 5f;
        public float MarkHeight = 8f;
        public int NumberOfGoals = 16;
        public int EgoMarkRelation = 6;
        public int AlloMarkRelation = 3;
        public string GoalObject = "BVARoundFloor";
        public string MarkObject = "BVAMarkCue";
        public string StartObject = "BVAMarkStart";
        public float GoalRadius = 15;
        public float MarkRadius = 20;
        public float StartRadius = 20;
        public bool PointAtFirst = true;
        public float TrialTimeLimit = 5.0f;
        public float TestTrialTimeLimit = 10.0f;
        public int CorrectTrialsToAdvance = 4;
        public int HideGoaleAfter = 2;
        public int MaxTypeInRow = 2;
        public int CorrectTrialsToFinish = 6;

        public EgoAlloTrainingSettings()
        {
            Messages = new Dictionary<string, string>
            {
                { "LearnWalking" , "Chůze probíhá pomocí tlačítek W, A, S, D. \n Otáčet se můžete myší."},
                { "LearnStart", "Mezerník odstrartuje úkol. \n Nicméně musíte stát u startu (červená tečka na stěně stanu) a dívat se na střed."},
                { "LearnPointing" , "Ukazovat budete před každým startem. Poznáte to podle zablokované chůze a červené tečky. \n Směr potvrdíte klávesou E nebo pravým tlačítkem myši."},
                { "LearnEgo", "V tomto typu úkolů je cíl vázaný na místo startu. \n Tyto úkoly poznáte dlouhého tónu."},
                { "LearnAllo", "V tomto typu úkolů je cíl vázaný ke značce na stěně. \n Tyto úkoly poznáte tří krátkých tónů."},
                { "TryAllo", "Teď už cíl nebude vidět. Cíl je stále vůči značce ve stejné pozici. \n Začněte nový úkol a zkuste jej najít."},
                { "LearnAll", "Teď už byste všechny typy úkolů měli zvládat. \n Postupně se budou střídat, ale nic nového nepřijde :)"},
                { "TryEgo", "Teď už cíl nebude vidět. Cíl je stále vůči vám ve stejné pozici \n Začněte nový úkol a zkuste jej najít."},
                { "FarFromStart", "Je třeba stát blízko u startu (červená tečka na stěně)."},
                { "NotCentered", "Je třeba dívat se do středu stanu. Můžete se zorientovat podle otvoru ve stropě."},
                { "PointFromStart", "Začněte úkol ze startu."},
                { "Point", "Ukažte na cíl"},
                { "Go", "Dojděte na cíl."},
                { "New", "Až budete připraveni, nový úkol začnete klávesou mezerník."}
            };
        }

#if UNITY_EDITOR
    [MenuItem("Assets/Experiment/EgoAlloTrainingSettings")]
        public static void CreateDialogueLine()
        {
            ScriptableObjectUtility.CreateAsset<EgoAlloTrainingSettings>();
        }
#endif
    }
}
