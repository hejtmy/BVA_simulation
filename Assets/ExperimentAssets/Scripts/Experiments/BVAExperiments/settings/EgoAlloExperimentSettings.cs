﻿using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityFramework.Helpers;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
#endif

namespace Assets.ExperimentAssets.Scripts.Experiments.BVAExperiments
{
    public class ExperimentEgoAlloSettings : ExperimentSettings
    {
        protected float ArenaSize = 20;
        public float GoalSize = 5f;
        public float MarkHeight = 8f;
        public int NumberOfGoals = 16;
        public int[] MarkOrder = { 11, 11, 11, 11, 7, 5, 15, 11, 3, 15, 9, 15, 1, 13, 9, 11, 3, 1, 7, 5, 13, 9, 5, 1 };
        public int[] StartOrder = { 7, 5, 3, 9, 1, 13, 7, 5, 1, 9, 5, 15, 1, 7, 3, 11, 13, 9, 3, 13, 11, 5, 15, 11 };
        public string[] RandomOrdering =
        {
            "Allo", "Ego", "Allo", "Ego", "Ego", "Allo", "Allo", "Ego", "Ego", "Ego", "Allo", "Ego", "Ego", "Ego", "Ego",
            "Allo", "Ego", "Allo", "Ego", "Ego", "Allo", "Allo", "Ego", "Ego", "Allo", "Ego", "Ego", "Allo", "Allo",
            "Allo", "Ego", "Ego", "Allo", "Allo", "Allo", "Ego", "Ego", "Allo", "Allo", "Allo"
        };
        public int TrainingLength = 4;
        public int EgoMarkRelation = 6;
        public int AlloMarkRelation = 3;
        public string GoalObject = "BVARoundFloor";
        public string MarkObject = "BVAMarkCue";
        public string StartObject = "BVAMarkStart";
        public float GoalRadius = 15;
        public float MarkRadius = 20;
        public float StartRadius = 20;
        public bool PointAtFirst = true;
        public ExperimentEgoAlloSettings()
        {
            ExperimentName = "ExperimentEgoAllo";
            Messages = new Dictionary<string, string>
            {
                {"FarFromStart", "Je třeba stát blízko u startu (červená tečka na stěně)" },
                {"PointFromStart", "Začněte úkol ze startu."},
                {"NotCentered", "Je třeba dívat se do středu stanu. Můžete se zorientovat podle díry ve stropě."},
                {"Point", "Ukažte na cíl"},
                {"Go", "Dojděte na cíl."},
                {"NewTrial", "Můžete začít nový úkol."},
                {"EndTrial", "Dokončeno. Pro další úkol stiskněte mezerník"}
            };
        }
  
#if UNITY_EDITOR
        [MenuItem("Assets/Experiment/ExperimentEgoAlloSettings")]
        public static void CreateDialogueLine()
        {
            ScriptableObjectUtility.CreateAsset<ExperimentEgoAlloSettings>();
        }
        [CustomEditor(typeof(ExperimentEgoAlloSettings))]
        public class SettingsEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();
                ExperimentEgoAlloSettings myScript = (ExperimentEgoAlloSettings)target;
                if (GUILayout.Button("Serialise settings"))
                {
                    Debug.Log(myScript.SerialiseOut());
                }
            }
        }
#endif
    }
}
