﻿using System;
using BrainVR.UnityFramework.Crosshair;
using BrainVR.UnityFramework.DataHolders;
using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityFramework.InputControl;
using BrainVR.UnityFramework.Player;
using BrainVR.UnityFramework.Scripts.Objects.Goals;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.ExperimentAssets.Scripts.Experiments.BVAExperiments
{
    public enum TrainingPhase
    {
        Walking,
        Pointing,
        Allo,
        Ego,
        All
    }
    public class EgoAlloTraining : BVAExperiments
    {
        public new EgoAlloTrainingSettings Settings;
        public TrialType CurrentTrialType;
        public TrainingPhase TrainingPhase;

        public int ConsecutiveCorrect;

        private float _trialStart;
        private float _trialFinish;
        private string _instructionText;
        private int _sameTypes;
        private int _trialsInPhase;

        #region Experiment flow
        protected override void OnExperimentInitialise()
        {
            base.OnExperimentInitialise();
            var currentSettings = SettingsHolder.Instance.CurrentExperimentSettings();
            ShouldLog = Settings.ShouldLog;
        }
        public override string ExperimentHeaderLog()
        {
            //deserialise 
            var str = "***EXPERIMENT SETTINGS***" + Environment.NewLine;
            str += Settings.SerialiseSettings();
            str += Environment.NewLine + "---EXPERIMENT SETTINGS---" + Environment.NewLine;
            str += "***POSITIONS***" + Environment.NewLine;
            var header = new TestHeader();
            foreach (var mark in MarkControllers)
            {
                header.MarkPositions.Add(mark.transform.position.ToString());
            }
            foreach (var mark in StartControllers)
            {
                header.StartPositions.Add(mark.transform.position.ToString());
            }
            foreach (var goal in GoalControllers)
            {
                header.GoalPositions.Add(goal.transform.position.ToString());
            }
            str += JsonConvert.SerializeObject(header, Formatting.Indented);
            str += Environment.NewLine + "---POSITIONS---" + Environment.NewLine;
            return str;
        }
        protected override void OnExperimentUpdate()
        {
            if (Input.GetButtonDown("ShowStart")) SwitchStart();
            if (Input.GetButtonDown("ShowGoal")) SwitchGoal();
            if (Input.GetButtonDown("Confirm")) Confirmed();
            if (Input.GetButtonDown("StartTrial") && TrialState.Equals(TrialState.WaitingToStart)) TrialStart();
            if (Input.GetButtonDown("NextTrial") && TrialState.Equals(TrialState.Finished)) TrialSetNext();
            if (Input.GetButtonDown("ForceNextTrial")) ForceNextTrial();
        }
        protected override void OnExperimentSetup()
        {
            base.OnExperimentSetup();

            GoalControllers = GoalManager.InstantiateGoalsCircumference(Settings.NumberOfGoals, radius: Settings.GoalRadius);
            MarkControllers = MarkManager.InstantiateObjectsOnCircleCircumference(Settings.NumberOfGoals, radius: Settings.MarkRadius);
            StartControllers = StartMarkManager.InstantiateObjectsOnCircleCircumference(Settings.NumberOfGoals, radius: Settings.StartRadius);

            MarkManager.SetType(Settings.MarkObject);
            StartMarkManager.SetType(Settings.StartObject);
            GoalManager.SetType(Settings.GoalObject);
            GoalManager.ResizeGoals(Settings.GoalSize);
            MarkManager.SetHight(Settings.MarkHeight);
            StartMarkManager.SetHight(Settings.MarkHeight);

            GoalManager.ShowAll(false);
            StartMarkManager.ShowAll(false);
            MarkManager.ShowAll(false);

            CanvasManager.SetName("Trial number", "Splněno:");
            TrainingPhase = TrainingPhase.Walking;
            SetInstructionText();
        }
        protected override void OnExperimentFinished()
        {
            CanvasManager.SetText("Instructions", "Trénink dokončen");
            CanvasManager.SetText("Trial number", "");
            CanvasManager.SetText("Message", "Výborně, stihli jste to v časovém limitu");
        }
        #endregion
        #region Trial flow
        protected override void OnTrialSetup()
        {
            if (TrainingPhase == TrainingPhase.Walking) SetupWalking();
            if (TrainingPhase == TrainingPhase.Pointing) SetupPointing();
            if (TrainingPhase == TrainingPhase.Ego) SetupEgo();
            if (TrainingPhase == TrainingPhase.Allo) SetupAllo();
            if (TrainingPhase == TrainingPhase.All) SetupAll();
        }
        protected override void AfterTrialSetup()
        {
            CanvasManager.SetText("Instructions", _instructionText);
            SetInstructionText();
            if (TrainingPhase == TrainingPhase.Walking) TrialStart();
        }
        protected override void OnTrialStart()
        {
            base.OnTrialStart();
            if (Arduino.SendPulsUp()) SendTrialEvent("ArduinoPulseStart");
            if (TrainingPhase == TrainingPhase.Walking) StartWalking();
            if (TrainingPhase == TrainingPhase.Pointing) StartPointing();
            if (TrainingPhase == TrainingPhase.Ego) StartEgo();
            if (TrainingPhase == TrainingPhase.Allo) StartAllo();
            if (TrainingPhase == TrainingPhase.All) StartAll();
            CanvasManager.SetText("Instructions", _instructionText);
        }
        protected override void AfterTrialStart()
        {
            _trialStart = Time.realtimeSinceStartup;
        }
        protected override void OnTrialFinished()
        {
            _trialFinish = Time.realtimeSinceStartup;
            base.OnTrialFinished();
            if (Arduino.SendPulsDown()) SendTrialEvent("ArduinoPulseStop");
            if (TrainingPhase == TrainingPhase.Walking) FinishWalking();
            if (TrainingPhase == TrainingPhase.Pointing) FinishPointing();
            if (TrainingPhase >= TrainingPhase.Allo) FinishAll();
            WasOnTime();
            _trialsInPhase++;
            BeepManager.Play("Finished");
            CanvasManager.SetText("Instructions", "Dokončeno.");
            CanvasManager.SetText("Trial number", _trialsInPhase.ToString());
        }
        protected void OnTrialForceFinished()
        {
            _trialStart = Time.realtimeSinceStartup; //basically makes it on time
        }
        protected override void AfterTrialFinished()
        {
            if (TrainingPhase <= TrainingPhase.Pointing) TrialSetNext();
            else CanvasManager.SetText("Instructions", Settings.Message("New"));
        }
        protected override void OnTrialClosed()
        {
            PhaseProgression();
            if (CurrentStart) CurrentStart.Show(false);
            if (CurrentMark) CurrentMark.Show(false);
            if (CurrentGoal) CurrentGoal.Show(false);
        }
        protected override void AfterTrialClosed() { }
        protected override bool CheckForEnd()
        {
            return TrainingPhase > TrainingPhase.All;
        }
        protected void PhaseProgression()
        {
            var canAdvance = (TrainingPhase < TrainingPhase.All & ConsecutiveCorrect >= Settings.CorrectTrialsToAdvance) || (TrainingPhase == TrainingPhase.All & ConsecutiveCorrect >= Settings.CorrectTrialsToFinish);
            if (!canAdvance) return;
            TrainingPhase ++;
            _trialsInPhase = 0;
            ConsecutiveCorrect = 0;
            CanvasManager.SetText("Trial number", "");
        }
        protected void SetInstructionText()
        {
            if (TrainingPhase == TrainingPhase.Walking) CanvasManager.SetText("Help", Settings.Message("LearnWalking"));
            if (TrainingPhase == TrainingPhase.Pointing) CanvasManager.SetText("Help", Settings.Message("LearnStart"));
            if (ConsecutiveCorrect >= 1 && TrainingPhase <= TrainingPhase.Pointing) CanvasManager.SetText("Help", "");
            if (TrainingPhase == TrainingPhase.Allo) CanvasManager.SetText("Help", ConsecutiveCorrect >= Settings.HideGoaleAfter ? Settings.Message("TryAllo") : Settings.Message("LearnAllo"));
            if (TrainingPhase == TrainingPhase.Ego) CanvasManager.SetText("Help", ConsecutiveCorrect >= Settings.HideGoaleAfter ? Settings.Message("TryEgo") : Settings.Message("LearnEgo"));
            if (TrainingPhase == TrainingPhase.All) CanvasManager.SetText("Help", ConsecutiveCorrect >= Settings.HideGoaleAfter ? "" : Settings.Message("LearnAll"));
            if (ConsecutiveCorrect >= Settings.CorrectTrialsToAdvance - 2) CanvasManager.SetText("Help", "");
        }
        #endregion
        #region Trial setups
        private void SetupWalking()
        {
            _instructionText = Settings.Message("Go");
            RandomizeGoal();
            CurrentGoal.OnEnter += GoalEnter;
        }
        private void SetupPointing()
        {
            _instructionText = Settings.Message("PointFromStart");
            RandomizeStart();
            RandomizeGoal();
            CurrentStart.Show();
        }
        private void SetupEgo()
        {
            _instructionText = Settings.Message("PointFromStart");
            var startPos = RandomizeStart();
            SetEgoGoal(startPos);
            CurrentStart.Show();
        }
        private void SetupAllo()
        {
            _instructionText = Settings.Message("PointFromStart");
            var startPos = RandomizeStart();
            SetAlloGoal(startPos);
            CurrentStart.Show();
        }
        private void SetupAll()
        {
            SetEgoAllo();
            if (CurrentTrialType == TrialType.Ego) SetupEgo();
            else SetupAllo();
        }
        #endregion
        #region Trial starts
        private void StartWalking()
        {
            CurrentGoal.Show();
            PlayerController.Instance.EnableMovement();
        }
        private void StartPointing()
        {
            if (ConsecutiveCorrect < Settings.HideGoaleAfter) CanvasManager.SetText("Help", Settings.Message("LearnPointing"));
            Pointing();
            CurrentGoal.Show();
        }
        private void StartEgo()
        {
            BeepManager.Play("Ego");
            Pointing();
        }
        private void StartAllo()
        {
            CurrentMark.Show();
            BeepManager.Play("Allo");
            Pointing();
        }
        private void StartAll()
        {
            if (CurrentTrialType == TrialType.Allo)
            {
                CurrentMark.Show();
                BeepManager.Play("Allo");
            }
            else BeepManager.Play("Ego");
            CrosshairController.Instance.SetColour(Color.red);
            PlayerController.Instance.EnableMovement(false);
            InputManager.PointButtonPressed += Point;
            _instructionText = Settings.Message("Point");
        }
        private void Pointing()
        {
            if (ConsecutiveCorrect < Settings.HideGoaleAfter) CurrentGoal.Show();
            CrosshairController.Instance.SetColour(Color.red);
            PlayerController.Instance.EnableMovement(false);
            InputManager.PointButtonPressed += Point;
            _instructionText = Settings.Message("Point");
        }
        #endregion
        #region Trial finishes
        private void FinishWalking()
        {
            CurrentGoal.OnEnter -= GoalEnter;
        }
        private void FinishPointing()
        {
            InputManager.PointButtonPressed -= Point;
            PlayerController.Instance.EnableMovement();
        }
        private void FinishAll()
        {
            CurrentGoal.OnEnter -= GoalEnter;
        }
        private void WasOnTime()
        {
            var diff = _trialFinish - _trialStart;
            var wasOnTime = (diff < Settings.TrialTimeLimit & TrainingPhase < TrainingPhase.All) ||
                           (diff < Settings.TestTrialTimeLimit & TrainingPhase == TrainingPhase.All);
            if (wasOnTime)
            {
                CanvasManager.SetText("Message", "Výborně, stihli jste to v časovém limitu");
                ConsecutiveCorrect++;
            }
            else
            {
                CanvasManager.SetText("Message",
                    "Jste pomalejší o " + Math.Round(diff - Settings.TrialTimeLimit, 1) +
                    " s. Zkuste to trochu rychleji.");
                ConsecutiveCorrect = 0;
            }
        }
        #endregion
        #region Trial functions
        private void Confirmed()
        {
            switch (TrialState)
            {
                case TrialState.Finished:
                    TrialSetNext();
                    break;
                case TrialState.WaitingToStart:
                    if (CheckIfTrialCanStart()) TrialStart();
                    break;
            }
        }
        private bool CheckIfTrialCanStart()
        {
            //is close to start?
            if (Vector2.Distance(PlayerController.Instance.Vector2Position, CurrentStart.PositionV2()) > 5)
            {
                CanvasManager.SetText("Help", Settings.Message("FarFromStart"));
                return false;
            }
            //is looking to the center?
            if (Vector3.Angle(PlayerController.Instance.transform.forward, transform.position - PlayerController.Instance.transform.position) > 20)
            {
                CanvasManager.SetText("Help", Settings.Message("NotCentered"));
                return false;
            }
            SetInstructionText();
            return true;
        }
        private void Point()
        {
            if (!PointsCorrectly(CurrentGoal.gameObject, 20))
            {
                CanvasManager.SetText("Message", "Neukázali jste přesně.");
                ConsecutiveCorrect = -1; //will get bumped by 1 or set to zero on finish
                CurrentGoal.Show();
                return;
            }
            CrosshairController.Instance.ResetColor();
            //order is importnat
            if (TrainingPhase >= TrainingPhase.Allo) ContinueTrial();
            if (TrainingPhase == TrainingPhase.Pointing) TrialFinish();
        }
        public bool PointsCorrectly(GameObject target, float allowedDifference)
        {
            var leveledTarget = new Vector3(target.transform.position.x, 0, target.transform.position.z);
            var leveledPlayer = new Vector3(PlayerController.Instance.transform.position.x, 0,
                PlayerController.Instance.transform.position.z);
            if (Vector3.Angle(leveledTarget - leveledPlayer, PlayerController.Instance.transform.forward) >
                allowedDifference) return false;
            return true;
        }
        private void ContinueTrial()
        {
            InputManager.PointButtonPressed -= Point;
            PlayerController.Instance.EnableMovement();
            Debug.Log("Subscribed");
            CurrentGoal.OnEnter += GoalEnter;
            CanvasManager.SetText("Instructions", Settings.Message("Go"));
        }
        bool CloseToStart()
        {
            return ObjectsCloseTogether(PlayerController.Instance.gameObject, CurrentStart.gameObject, 10);
        }
        // CUSTOM FUNCTIONS FOR SPECIFIC EXPERIMENT
        private void GoalEnter(GoalController sender, EventArgs e)
        {
            if (TrialState == TrialState.Running) TrialFinish();
        }
        protected void SwitchGoal()
        {
            string message = CurrentGoal.Switch() ? "GoalDisplayed" : "GoalHidden";
            SendTrialEvent(message);
        }
        protected void SwitchStart()
        {
            var message = CurrentStart.Switch() ? "StartDisplayed" : "StartHidden";
            SendTrialEvent(message);
        }
        #endregion
        #region Trial helpers
        private void RandomizeGoal()
        {
            var which = UnityEngine.Random.Range(0, Settings.NumberOfGoals - 1);
            CurrentGoal = GoalControllers[which];
        }
        private void SetEgoAllo()
        {
            var lastTrialType = CurrentTrialType;
            CurrentTrialType = UnityEngine.Random.Range(0, 2) == 0 ? TrialType.Ego : TrialType.Allo;
            //dont want too many same type in a row
            if (CurrentTrialType == lastTrialType) _sameTypes += 1;
            if (_sameTypes <= Settings.MaxTypeInRow) return;
            CurrentTrialType = OtherTrialType(CurrentTrialType);
            _sameTypes = 0;
        }
        private void SetEgoGoal(int startPosition)
        {
            var position = PositionInCircle(startPosition + Settings.EgoMarkRelation);
            CurrentGoal = GoalControllers[position];
        }
        private void SetAlloGoal(int startPosition)
        {
            var markPosition =
                PositionInCircle(startPosition + Settings.NumberOfGoals/2 + UnityEngine.Random.Range(-2, 2));
            CurrentMark = MarkControllers[markPosition];
            var position = PositionInCircle(markPosition + Settings.AlloMarkRelation);
            CurrentGoal = GoalControllers[position];
        }
        private int RandomizeStart()
        {
            var which = UnityEngine.Random.Range(0, Settings.NumberOfGoals - 1);
            CurrentStart = StartControllers[which];
            return which;
        }
        public bool ObjectsCloseTogether(GameObject go1, GameObject go2, float distance)
        {
            if (!go1 || !go2) return false;
            if (Vector2.Distance(new Vector2(go1.transform.position.x, go1.transform.position.z),
                new Vector2(go2.transform.position.x, go2.transform.position.z)) < distance) return true;
            return false;
        }
        private int PositionInCircle(int position)
        {
            return position >= Settings.NumberOfGoals ? position - Settings.NumberOfGoals : position;
        }
        private static TrialType OtherTrialType(TrialType type)
        {
            return type == TrialType.Allo ? TrialType.Ego : TrialType.Allo;
        }

        public override void AddSettings(ExperimentSettings settings)
        {
            Settings = (settings != null ? (EgoAlloTrainingSettings)settings : null) ?? new EgoAlloTrainingSettings();
        }
        #endregion
    }
}
