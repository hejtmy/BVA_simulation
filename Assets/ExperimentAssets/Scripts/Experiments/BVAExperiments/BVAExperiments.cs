﻿using BrainVR.UnityFramework.Arduino;
using BrainVR.UnityFramework.DataHolders;
using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityFramework.Objects.Goals;
using BrainVR.UnityFramework.Objects.Marks;
using BrainVR.UnityFramework.Scripts.Objects.Beeper;
using BrainVR.UnityFramework.Scripts.Objects.Goals;
using BrainVR.UnityFramework.Scripts.Objects.Marks;
using BrainVR.UnityFramework.UI.InGame;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.ExperimentAssets.Scripts.Experiments.BVAExperiments
{
    public abstract class BVAExperiments : BaseExperiment
    {
        protected float arenaSize = 20;
        //maybe more to some other abstract class
        protected MarkManager MarkManager;
        protected StartManager StartMarkManager;
        protected GoalManager GoalManager;
        protected BeeperManager BeepManager;
        protected ExperimentCanvasManager CanvasManager;

        protected List<GoalController> GoalControllers;
        protected List<MarkController> MarkControllers;
        protected List<MarkController> StartControllers;
        protected ArduinoController Arduino;

        protected GoalController CurrentGoal;
        protected MarkController CurrentMark;
        protected MarkController CurrentStart;

        protected float TrialStartTime;
        protected float TrialEndTime;
        protected List<float> TrialTimes = new List<float>();

        #region Experiment Logic
        protected override void OnExperimentInitialise()
        {
            MarkManager = MarkManager.Instance;
            StartMarkManager = StartManager.Instance;
            GoalManager = GoalManager.Instance;
            BeepManager = BeeperManager.Instance;
            Arduino = ArduinoController.Instance;
            AddSettings(SettingsHolder.Instance.CurrentExperimentSettings());
        }
        protected override void OnExperimentUpdate() { }
        protected override void OnExperimentSetup()
        {
            CanvasManager = ExperimentCanvasManager.Instance;
            CanvasManager.Show();
            //Arduino.Connect();
        }
        protected override void AfterExperimentSetup() { }
        protected override void OnExperimentStart()
        {
            TrialNumber = 0;
        }
        protected override void AfterExperimentStart() { }
        protected override void OnTrialSetup() { }
        protected override void OnTrialStart()
        {
            TrialStartTime = Time.realtimeSinceStartup;
        }
        protected override void OnTrialFinished()
        {
            TrialEndTime = Time.realtimeSinceStartup;
            TrialTimes.Add(TrialEndTime-TrialStartTime);
        }
        protected override void OnTrialClosed() { }
        protected override void OnExperimentFinished() { }
        protected override void AfterExperimentFinished() { }
        protected override void OnExperimentClosed()
        {
            CanvasManager.Show(false);
            //we are actually destroying this one because it is the only one instantiated
            //start manager is mark manager, but that one is a singleton, so we cant have that in 
            //a scene beforehand, it woudl screw up stuff, so we instantiate and destroy it

            //TODO it doesn't get destroyed second time around, don't kwno why
            Destroy(StartMarkManager.gameObject);
        }
        protected override void AfterExperimentClosed() { }
        #endregion
        #region Helpers
        #endregion
    }
}
