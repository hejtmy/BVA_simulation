﻿using System;
using System.Collections.Generic;
using BrainVR.UnityFramework.Crosshair;
using BrainVR.UnityFramework.Experiment;
using BrainVR.UnityFramework.InputControl;
using BrainVR.UnityFramework.Player;
using BrainVR.UnityFramework.Scripts.Objects.Goals;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.ExperimentAssets.Scripts.Experiments.BVAExperiments
{
    public enum TrialType
    {
        Allo,
        Ego,
        Other
    }
    public class TestHeader
    {
        public List<string> GoalPositions = new List<string>();
        public List<string> MarkPositions = new List<string>();
        public List<string> StartPositions = new List<string>();
    }
    public class ExperimentEgoAllo : BVAExperiments
    {
        public new ExperimentEgoAlloSettings Settings;
        public TrialType CurrentTrialType;

        //there is not everything because we implemet default versions from the parent classes
        protected override void OnExperimentInitialise()
        {
            base.OnExperimentInitialise();
            ShouldLog = Settings.ShouldLog;
        }
        public override string ExperimentHeaderLog()
        {
            //deserialise 
            var str = "***EXPERIMENT SETTINGS***" + Environment.NewLine;
            str += Settings.SerialiseSettings();
            str += Environment.NewLine + "---EXPERIMENT SETTINGS---" + Environment.NewLine;
            str += "***POSITIONS***" + Environment.NewLine;
            var header = new TestHeader();
            foreach (var mark in MarkControllers)
            {
                header.MarkPositions.Add(mark.transform.position.ToString());
            }
            foreach (var mark in StartControllers)
            {
                header.StartPositions.Add(mark.transform.position.ToString());
            }
            foreach (var goal in GoalControllers)
            {
                header.GoalPositions.Add(goal.transform.position.ToString());
            }
            str += JsonConvert.SerializeObject(header, Formatting.Indented);
            str += Environment.NewLine + "---POSITIONS---" + Environment.NewLine;
            return str;
        }
        protected override void OnExperimentUpdate()
        {
            if (Input.GetButtonDown("ShowStart")) SwitchStart();
            if (Input.GetButtonDown("ShowGoal")) SwitchGoal();
            if (Input.GetButtonDown("Confirm")) Confirmed();
            if (Input.GetButtonDown("StartTrial") && TrialState.Equals(TrialState.WaitingToStart)) TrialStart();
            if (Input.GetButtonDown("NextTrial") && TrialState.Equals(TrialState.Finished)) TrialSetNext();
            if (Input.GetButtonDown("ForceNextTrial")) ForceNextTrial();
        }
        /// <summary>
        /// Does variety of things based on the state of the game
        /// </summary>
        private void Confirmed()
        {
            switch (TrialState)
            {
                case TrialState.Finished:
                    TrialSetNext();
                    break;
                case TrialState.WaitingToStart:
                    if (CheckIfTrialCanStart()) TrialStart();
                    break;
            }
        }
        /// <summary>
        /// Checks several donditions for the trial to be started. Player position, rotation, game state etc.
        /// </summary>
        /// <returns></returns>
        private bool CheckIfTrialCanStart()
        {
            //is close to start?
            if (Vector2.Distance(PlayerController.Instance.Vector2Position, CurrentStart.PositionV2()) > 10)
            {
                CanvasManager.SetText("Help", Settings.Message("FarFromStart"));
                return false;
            }
            //is looking to the center?
            if (Vector3.Angle(PlayerController.Instance.transform.forward, transform.position - PlayerController.Instance.transform.position) > 20)
            {
                CanvasManager.SetText("Help", Settings.Message("NotCentered"));
                return false;
            }
            CanvasManager.HideField("Help");
            return true;
        }
        private bool CorrectFacing(GameObject target, float allowedDifference)
        {
            var leveledTarget = new Vector3(target.transform.position.x, 0, target.transform.position.z);
            var leveledPlayer = new Vector3(PlayerController.Instance.transform.position.x, 0, PlayerController.Instance.transform.position.z);
            if (Vector3.Angle(leveledTarget - leveledPlayer, PlayerController.Instance.transform.forward) > allowedDifference) return false;
            return true;
        }
        protected override void OnExperimentSetup()
        {
            base.OnExperimentSetup();
            GoalControllers = GoalManager.InstantiateGoalsCircumference(Settings.NumberOfGoals, radius:Settings.GoalRadius);
            MarkControllers = MarkManager.InstantiateObjectsOnCircleCircumference(Settings.NumberOfGoals, radius: Settings.MarkRadius);
            StartControllers = StartMarkManager.InstantiateObjectsOnCircleCircumference(Settings.NumberOfGoals, radius: Settings.StartRadius);
            MarkManager.SetType(Settings.MarkObject);
            StartMarkManager.SetType(Settings.StartObject);
            GoalManager.SetType(Settings.GoalObject);
            GoalManager.ResizeGoals(Settings.GoalSize);
            MarkManager.SetHight(Settings.MarkHeight);
            StartMarkManager.SetHight(Settings.MarkHeight);

            GoalManager.ShowAll(false);
            StartMarkManager.ShowAll(false);
            MarkManager.ShowAll(false);
            //Connects the arduino
            Arduino.Connect();
        }
        protected override void OnTrialSetup()
        {
            CurrentMark = MarkControllers[Settings.MarkOrder[TrialNumber]];
            CurrentStart = StartControllers[Settings.StartOrder[TrialNumber]];
            CurrentGoal = GetCurrentGoal();
            CurrentTrialType = GetCurrentTrialType();
            CanvasManager.SetText("Instructions", Settings.Message("NewTrial"));
            CanvasManager.SetText("Trial number", (TrialNumber + 1).ToString());
            CurrentStart.Show();
            PlayerController.Instance.EnableMovement();
        }
        protected override void OnTrialStart()
        {
            base.OnTrialStart();
            if(Arduino.SendPulsUp()) SendTrialEvent("ArduinoPulseStart");
            BeepManager.Play(Settings.RandomOrdering[TrialNumber]);
            if (Settings.TrainingLength > TrialNumber) CurrentGoal.Show();
            CurrentStart.Show(false);
            if (CurrentTrialType == TrialType.Allo) CurrentMark.Show();
            if (Settings.PointAtFirst) PointToTarget();
            else ContinueTrialStart();
        }
        void PointToTarget()
        {
            PlayerController.Instance.EnableMovement(false);
            CrosshairController.Instance.SetColour(Color.red);
            CanvasManager.SetText("Instructions", Settings.Message("Point"));
            InputManager.PointButtonPressed += EndPointing;
            InputManager.PointButtonPressed += ContinueTrialStart;
        }
        private void EndPointing()
        {
            CrosshairController.Instance.ResetColor();
            InputManager.PointButtonPressed -= ContinueTrialStart;
        }
        void ContinueTrialStart()
        {
            CurrentGoal.Show(false);
            PlayerController.Instance.EnableMovement();
            CanvasManager.SetText("Instructions", Settings.Message("Go"));
            CurrentGoal.OnEnter += GoalEnter;
        }
        protected override void OnTrialFinished()
        {
            base.OnTrialFinished();
            if (Arduino.SendPulsDown()) SendTrialEvent("ArduinoPulseStop");
            CurrentGoal.OnEnter -= GoalEnter;
            BeepManager.Play("Finished");
            CanvasManager.SetText("Instructions", Settings.Message("EndTrial"));
            CurrentGoal.Show();
            if (CurrentTrialType == TrialType.Ego) CurrentStart.Show();
        }
        protected override void OnTrialClosed()
        {
            CurrentStart.Show(false);
            CurrentMark.Show(false);
            CurrentGoal.Show(false);
        }
        protected override bool CheckForEnd()
        {
            return TrialNumber == Settings.StartOrder.Length - 1;
        }
        // CUSTOM FUNCTIONS FOR SPECIFIC EXPERIMENT
        private GoalController GetCurrentGoal()
        {
            var position = Settings.RandomOrdering[TrialNumber] == "Ego"? Settings.StartOrder[TrialNumber] + Settings.EgoMarkRelation : Settings.MarkOrder[TrialNumber] + Settings.AlloMarkRelation;
            //corrects for circling around
            position = position >= Settings.NumberOfGoals ? position - Settings.NumberOfGoals : position;
            return GoalControllers[position];
        }
        private void GoalEnter(GoalController sender, EventArgs e)
        {
            if (TrialState == TrialState.Running) TrialFinish();
        }
        protected void SwitchGoal()
        {
            string message = CurrentGoal.Switch() ?  "GoalDisplayed": "GoalHidden";
            SendTrialEvent(message);
        }
        protected void SwitchStart()
        {
            string message = CurrentStart.Switch() ? "StartDisplayed" : "StartHidden";
            SendTrialEvent(message);
        }
        private TrialType GetCurrentTrialType()
        {
            switch (Settings.RandomOrdering[TrialNumber])
            {
                case "Ego":
                    return TrialType.Ego;
                case "Allo":
                    return TrialType.Allo;
            }
            return TrialType.Other;
        }
        public override void AddSettings(ExperimentSettings settings)
        {
            Settings = (ExperimentEgoAlloSettings)settings;
        }
    }
}