# Overview
This experiment is designed to test allocentric and egocentric navigation as is tested in the Blue Velvet Arena in the Faculty hospital in Motol. It is generally administered in two phases - learning phase and experiment phase. During training participants learn position of allocentric and egocentric goals as well as procedure of the expeirment as a whole - how to walk, how to point etc. During the experiment, participant repeats the same task again and again, so there should be no confusion, if the training  went fine.

Each trial has a specific structure:

1. Go to the start
1. Orient towards the center and confirm start readiness (spacebar)
1. Sound is played which determines ego or allocentric goal
1. Point towards the goal (right mouse)
1. Walk towards the goal. When goal is reached, new trial finishes
1. After participant wants to proceed to next trial, press spacebar
1. New trial begins

# What to do before
For technical information, how to start and setup the expeirment, please refer to the readme.md file. It should have answers to all questions about setting up the PC, initialising connection to the synchronisation device, where to put config files, keyboard shortcuts etc. This file presumes you read and are familiar with the technical side of things.

# Terminology 
Goal = red circle on the floor. Until trial is finished, usually invisible.
Mark = three lines on a wall. Defines landmark during allocentric task
Start = red dot on a wall. Defines place to start
Center = imaginary position in the center of the tent. Can be defined by the ceiling hole or general layout of the room

Some helpful points are noted in *italics* whereas instructions mainly in **bold**.

# How to start
Setup the experiment as instructed in the readme.md. Put in participant name  and load training first. Start the training.

*It is nice gesture to the participant to inform them at all times what you are doing. If anything doens't work, just say what is wrong. Nothing is worse than you playing with a PC or a strange box while participant patiently waits five minutes unsure of what is happening. Explain why you are doing what you are doing. After all, they are giving you their free time. this experiment is BOOOORING, don't think you are doing them any favours :)*

# Before training
After you setup training trial and load the level, give the participant minute or two to walk around to familiarise themselves with the controls. If they are unsure, what to do, instruct them about WSAD keys and usage of mouse as best you see fit. If they are familiar with PC games, you can just skip forward.

*You should be familiar with all the controls as well, especially if participant presses some weird key by accident, you should know how to revert it.*

# Training
Training consists of 5 separate phases. In order for the participant to advance to the next pahse, participant has to do specific number of correct trials within specifid time limit. Both of these are defined in the expeirment settings configuration file. If participant doesn't finish trial on time or they answer  incorrectly, the phase counter resets. Therefore participant has to keep repeating the phase until they are capable in finishing it without mistakes and on time. 

Each phase also has several "guiding" trials - they explicitely instruct participant what to do. Number of guiding trials can also be set int he config file. these get repeated if the participant has to repeat the phase. You can easilly distinguish these guidance trials, as they provide clear instructions on the screen and usually display goals as well.

**Instruct participant about the training phase and how it is going to proceed. Remind them, that time is as important as  accuracy and they will have to repeat each task until they are able to finish it accurately and fast. Ask them to ask any questions if they don't understand any part of the procedure. Also inform them about different GUI elements - what is which counter and instruction**

## Walking phase
During this phase, participant is only asked to walk to goals as they appear on the floor. They need to finish each walking trial within time limit. This poses no problem to participants used to FPS games, but some participants might struggle.

**Instruction is provided on the screen. Help participant to understand what is meant by the "goal" and how to use WSAD and mouse agin, if necessary**

## Pointing phase
During this phase, participant needs to move to the start and initiate new trial. New trial can only be initiated when participant stands at the start and is oriented toward the center. If these conditions are satisfied, they can press Confirm button (usually set to spacebar). They are then asked to point towards a visible goal. Pointing is done with the right mouse click or Point key (usually E)

**Instruct participants only if necessary. The instruction points shoudl be clear enough. When participant is asked to point, instruct them what they should do and what key/mouse is the pointing one.**

After pointing is fulfilled, new start is shown. Participant should repeat the phase until they understand how to wal to start and point.

## Allocentric phase
This phase combines pointing with walking as well as teaches position of allocentric goal. Participant first has to go to the start and initiate the pointing just as in the previous phase. After the pointing is initiated, allocentric sound and mark are shown. In the guidance trials, goal is shown as well to teach participant how they are position in respect to the mark.

**Explain the property of the allocentric goal and how it relates to the mark on the wall. Also explain, that despite the goal is visible at this time, during the expeirment if will remain invisible and their task is to find it, so they should start learning its position.**

After explanation is provided, participant is asked to point and then walk to the goal. Goal hides after the pointing, but is still at the same position. If participant points incorrectly, it resets the counter.

**Ask participant to walk to the goal.**

After the goal is reached, mark and goal appear again. If participant took long or made a mistake, now they can reflect the position of the goal in respect to the landmark and improve in future trials. After they are ready, they can proceed.

**It shoudl be repeated to the participant again, how are allocentric goals positioned. After they are ready, they can start new trial by pressin spacebar. Keep instructing them only if they seem genuinly lost for long periods of time or they keep making the same mistake.**

If participant doesn't understnad the concept, try to explain in other terms.

**If participant is slow or failing the task, remind them that they need to finish N trials on time and correctly, until you can progress**

## Egocentric phase
This phase initiates only when allocentric pahse is finished. Participant should be fluent in the "go to start - confirm - point - go to goal - start new trial" procedure. They are now learning the egocentric goal position. 

**Instruct participant to start thte trial as usual. When they do, explain the new position of the goal and why is the allocentric mark missing. Explain, that the goal is now bound to their starting position and view, not to external larmark.**

Repeat in the same manner as previous phase. If in doubt, ask if participant understands the egocentric vs allocentric goal.

## Final training phase
This phase represents how the experiment will run, with the positions of goals unchanged as well. First N trials are guidance trials with visible goals, but the rest are as usual.

**Inform participant that the last part of the training is exactly as the experiemnt will occur. Egocentric and allocentric goals are randomly seleted and they are asked to find each of them wihin time limit.** 

## After training
After trainign is finished, go back to the menu, load and start the experiment.

# Experiment phase

**Remind participant that the expeirment is now exactly as the training was, including all positions and procedures.**

*Experiment has currently 49 trials (check before :) just in case), keep reminding them how they are doing and how many are before them, if they seem bored.*

# Notes
Some participants finish the expeirment really fast, whereas others take almost extrmely long periods of time. Learning is supposed to take long periods fo time. If participants are doing some phase over and over, try to better explain how to fulfill it. 

E.g. one participant was doing the egocentric phase 4 times, before she finally understood how this navigation works. Be supportive and ask if they need help. It was necessary to remind her that the phase kept resetting and that is why she was doing the same trial again adn again.